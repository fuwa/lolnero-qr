# Copyright (c) 2019-2020, The Wownero Project
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are
# permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of
#    conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list
#    of conditions and the following disclaimer in the documentation and/or other
#    materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be
#    used to endorse or promote products derived from this software without specific
#    prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
# THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
# THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

let
  nixpkgs = import <nixpkgs> {}

; android-studio-deps = with nixpkgs;
  [
    coreutils
    findutils
    file
    git
    gn
    gnused
    gnutar
    glib
    gzip
    pciutils
    unzip
    which
    xkeyboard_config
  ]

; vendorCmake = nixpkgs.libsForQt5.callPackage ./deps/cmake {}

; ndk-r21 =
    let
      version = "r21e"
    ; in
    nixpkgs.fetchzip
    {
      url = "https://dl.google.com/android/repository/android-ndk-${version}-linux-x86_64.zip"
    ; sha256 = "10gv70bblifypx1r4qy771p92amxaq3yrbd7j5fzmipdyar5hs9h"
    ; }

; ndk-r23 =
    let
      version = "r23-beta3"
    ; in
    nixpkgs.fetchzip
    {
      url = "https://dl.google.com/android/repository/android-ndk-${version}-linux-x86_64.zip"
    ; sha256 = "12gcg8w7ybxlqa034jawc364d084wq2bl9rsz210l1hayp1y7xg8"
    ; }

; openssl-source =
    let
      name = "openssl"
    ; version = "1.1.1k"
    ; in
    nixpkgs.fetchurl
    {
      url = "https://www.openssl.org/source/${name}-${version}.tar.gz"
    ; sha256 = "1rdfzcrxy9y38wqdw5942vmdax9hjhgrprzxm42csal7p5shhal9"
    ; }

; boost-version = "1_76_0"
; boost-source =
    let
      name = "boost"
    ; version = boost-version
    ; dot_version = "1.76.0"
    ; in
    nixpkgs.fetchurl
      {
        url =
        "https://dl.bintray.com/boostorg/release/${dot_version}/source/${name}_${version}.tar.bz2"
      # ; sha256 = "1js9zpij58l60kx46s3lxdp5207igppjnhqigwhbpdvd04gb6gcm"
      ; sha256 = "0hcc661savk32hx65997p0ss1awj6ala4cmz4w7lbi42x6k7nfgh"
      ; }

; sodium-source =
    let
      name = "libsodium"
    ; version = "1.0.18"
    ; in
    nixpkgs.fetchurl
    {
      url = "https://github.com/jedisct1/${name}/archive/${version}.tar.gz"
    ; sha256 = "1x6lll81z4ah732zwpw481qfbzg7yml0nwdgbnd5388jnz3274ym"
    ; }


; lolnero-rev = "v0.9.7.5"
; lolnero-sha256 = "1j86qxci30r92zarwzf956a9fjpyiwpz5fw2igscflqrmsl99iih"

; lolnero-source =
    nixpkgs.fetchgit
    {
      url = "https://gitlab.com/lolnero/lolnero.git"
    ; rev = lolnero-rev
    ; sha256 = lolnero-sha256
    ; fetchSubmodules = false
    ; }

; in

with nixpkgs;

(buildFHSUserEnv {
  name = "lolnode-env"
; targetPkgs = pkgs: (with pkgs;
  [
    bash
    git
    curl
    unzip
    libGLU
    which

    zsh
    # jdk8 for sdkmanager
    jdk8

    # jdk for android dev
    # jdk

    # dart_dev
    gnumake
    gcc
    entr
    # androidenv.androidPkgs_9_0.platform-tools


    zlib
    ncurses
    # gcc
    libtool
    autoconf
    automake
    gnum4
    pkgconfig
    vendorCmake
    ccache

    python2
    ninja
  ]
  ++ android-studio-deps
  )

; multiPkgs = pkgs: (with pkgs;
  [
  ])


; profile = ''
    export ANDROID_HOME=~/local/sdk/android

    PATH=~/local/sdk/flutter/stable/bin:$PATH
    PATH=$ANDROID_HOME/cmdline-tools/latest/bin:$PATH

    export ANDROID_NDK_VERSION=r21
    export ANDROID_NDK_ROOT=${ndk-r21}
    export NDK=$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64
    PATH=$NDK/bin:$PATH

    export SRC_OPENSSL=${openssl-source}
    export SRC_BOOST=${boost-source}
    export SRC_SODIUM=${sodium-source}
    export SRC_RAPIDJSON_DIR=${nixpkgs.rapidjson.src}
    export SRC_LOLNERO_DIR=${lolnero-source}
    export VERSIONTAG_LOLNERO=${lolnero-rev}
    export NIX_BOOST_VERSION=${boost-version}

    export NIX_BUILD_TARGET=lolnerod

    export PATH_NCURSES=${nixpkgs.ncurses5}
    export PATH

    export _JAVA_AWT_WM_NONREPARENTING=1
    export DART_VM_OPTIONS=--root-certs-file=/etc/ssl/certs/ca-certificates.crt

    export ANDROID_NDK_VERSION_LOL=r23
    export ANDROID_NDK_ROOT_LOL=${ndk-r23}

    export ZSH_INIT=${nixpkgs.oh-my-zsh}/share/oh-my-zsh/oh-my-zsh.sh
    exec zsh
  ''

; }).env
