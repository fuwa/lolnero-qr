import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:android_intent_plus/android_intent.dart';

import 'package:logging/logging.dart';

final Logger log = Logger('Default');

void main() {
  Logger.root.level = kReleaseMode ? Level.INFO : Level.FINE;
  Logger.root.onRecord.listen((record) {
    print('${record.level.name}: ${record.time}: ${record.message}');
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      home: MyHomePage(title: 'L QR'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

bool isAddress(final String address) {
  if (address.length != 98) return false;

  if (address.startsWith('haha')) return true;
  if (address.startsWith('hoho')) return true;

  return false;
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    scanQR();
  }

  Future<void> scanQR() async {
    String _qr = '';
    final String crtGreen = '#33ff33';

    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      log.fine('trying scanning');
      _qr = await FlutterBarcodeScanner.scanBarcode(
          crtGreen, 'Cancel', true, ScanMode.QR);
      log.fine('scanned: $_qr');
    } on PlatformException {
      SystemNavigator.pop();
    }

    if (!mounted) return;

    if (isAddress(_qr)) {
      final _url = Uri(
        scheme: 'lolnero',
        host: 'resolve',
        queryParameters: {'address': _qr},
      ).toString();

      log.fine('url: $_url');

      final AndroidIntent intent = AndroidIntent(
        action: 'action_view',
        data: _url,
        package: 'org.lolnero.lolnero_wallet',
      );
      await intent.launch();
    }

    SystemNavigator.pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
